#include <Arduino.h>

#include <WiFi.h>
#include <WiFiMulti.h>

#include <HTTPClient.h>

#define USE_SERIAL Serial

WiFiMulti wifiMulti;

// Pin number we're getting flash data from
const int PIN = 12;
// milliseconds in 24 hours
const unsigned long MILLIS_IN_DAY = 86400000;
// generated in last time span
int wh = 0;
// generated in last 24 hours
int total = 0;
// timestamp for timespan
unsigned long since = millis();
// timespan for 24 hours
unsigned long start = millis();

void setup() {

    USE_SERIAL.begin(115200);
    pinMode(PIN, INPUT);
    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }

    wifiMulti.addAP("SSID", "RouterPassword");
}

void ping(){
      // wait for WiFi connection
    if((wifiMulti.run() == WL_CONNECTED)) {

        HTTPClient http;

        http.begin("http://192.168.0.4:82/generation2"); //HTTP
        http.addHeader("Content-Type", "application/json");

        // start connection and send HTTP header
        int httpCode = http.POST("{\"wh\": " + String(wh) + ", \"total\":" + String(total) + ", \"since\":" + String(millis()) + "}");

        http.end();
    }
}

int verifyFlash() {
  unsigned long flashStart = millis();
  // Wait a maximum of one second for the light to go out
  // Consider a flash if it doees or idle if not
  while (digitalRead(PIN) == 0 && millis() - flashStart < 1000) {
    delay(1);
  }

  return digitalRead(PIN);
}

void loop() {
  if (digitalRead(PIN) == 0) {
    // Could be a flash or idle state - wait for read to go back to 1
    if (verifyFlash() == 1) {
      // 1wh generated
      wh++;
      total++;
    }
  }

  if (millis() > since + 60000) {
      ping();
      wh = 0;
      since = millis();
  }

  if (millis() > start + MILLIS_IN_DAY) {
    total = 0;
    start = millis();
  }
}
